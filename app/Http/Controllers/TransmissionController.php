<?php

namespace App\Http\Controllers;

use Transmission\Client;
use Transmission\Transmission;
use Illuminate\Http\Request;
use Transmission\Model\Torrent;

class TransmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Transmission $transmission)
    {
        $torrents = $transmission->all();
        clock($torrents);
        return view('transmission.index')->with(compact('torrents'));
    }

    public function start(Transmission $transmission, $id)
    {
        $torrent = $transmission->get($id);
        $transmission->start($torrent);
        return redirect()->route('voyager.transmission.index');
    }

    public function stop(Transmission $transmission, $id)
    {
        $torrent = $transmission->get($id);
        $transmission->stop($torrent);
        return redirect()->route('voyager.transmission.index');
    }
}
