<?php

namespace App\Models;

use Watson\Rememberable\Rememberable;
use Illuminate\Database\Eloquent\Model;

class FeedCategory extends Model
{
    use Rememberable;

    public $timestamps = false;
    protected $fillable = ['id', 'name'];

    public function feeds()
    {
        return $this->hasMany('App\Models\Feed');
    }
}
