<?php

namespace App\Providers;

use Transmission\Client;
use Transmission\Transmission;
use Illuminate\Support\ServiceProvider;

class TransmissionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Transmission::class, function ($app) {
            $config = config('transmission.config');
            $client = new Client($config['url'], $config['port']);
            $client->getClient()->setTimeout(100000);
            $client->authenticate($config['user'], $config['password'], '/transmission/rpc');
            //$transmission = new Transmission($config->url, $config->port, 'transmission/rpc');
            $transmission = new Transmission();
            $transmission->setClient($client);
            return $transmission;
        });
    }
}
