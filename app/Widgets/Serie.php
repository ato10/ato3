<?php

namespace App\Widgets;

use App\Models\FeedCategory;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;

class Serie extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $cat_id = 29;
        $count = \DB::table('feeds')->where('category_id', $cat_id)->count();
        $cat = FeedCategory::findOrFail($cat_id);
        $string = $cat->name;

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-tv',
            'title'  => "{$count} {$string}",
            'text'   => "You have $count $string in your database",
            'button' => [
                'text' => "View all $string",
                'link' => url('admin/feeds?c=' . $cat_id),
            ],
            'image' => url('storage/widgets/series.jpg'),
        ]));
    }
}
