<?php

return [

    'config' => [
        'url' => env('TRANSMISSION_URL', 'localhost'),
        'port' => env('TRANSMISSION_PORT', 9091),
        'user' => env('TRANSMISSION_USER', 'admin'),
        'password' => env('TRANSMISSION_PASSWORD', 'admin'),
    ],

];
