<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return redirect('admin');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('torrent', 'TransmissionController@index')->name('voyager.transmission.index');
    Route::get('torrent/{id}/start', 'TransmissionController@start')->name('voyager.transmission.start');
    Route::get('torrent/{id}/stop', 'TransmissionController@stop')->name('voyager.transmission.stop');
});

Route::group(['prefix' => 'admin', 'middleware' => 'admin.user'], function () {
    Route::get('feeds', 'FeedController@index')->name('voyager.feeds.index');
    Route::get('feeds/{slug}', 'FeedController@show')->name('voyager.feeds.show');
    Route::get('starcitizen', 'StarcitizenController@index')->name('voyager.starcitizen.index');
    Route::get('starcitizen/{slug}', 'StarcitizenController@show')->name('voyager.starcitizen.show');
    /*Route::get('starcitizen', function () {
        $feed = new \SimplePie();
        $feed->set_cache_location(storage_path('framework/cache/'));
        $feed->set_feed_url("https://starcitizen.it/feed/");
        $feed->init();
        $feed->handle_content_type();
        //dump($feed);
        return view('starcitizen.index', [
            'feed'   => $feed
        ]);
    });*/
});
