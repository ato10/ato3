<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('test:mail', function () {
    $title = "Test email";
    $content = "Testo della mail";
    Mail::send('emails.test', ['title' => $title, 'content' => $content], function ($message)
    {
        $message->from('al.biasi@gmail.com', 'Alessandro Biasi');
        $message->to('al.biasi@gmail.com');
        $message->subject('Test email');
    });

})->describe('Invia una mail di prova');
