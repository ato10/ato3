@extends('voyager::master')

@section('page_title', __('voyager.generic.viewing').' Starcitizen.it')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <!--<i class="icon voyager-window-list"></i> {!! $feed->get_title() !!}-->
            <img src="{!! $image !!}"> {!! $feed->get_title() !!}
        </h1>
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-bordered">
            <div class="panel-body table-responsive">
                <div class="sc-item">
                    <small>{{ $feed->get_date() }}</small>
                    <p>{!! $feed->get_content() !!}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
@parent
@stop

@section('javascript')
@parent
@stop
