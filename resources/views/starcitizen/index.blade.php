@extends('voyager::master')

@section('page_title', __('voyager.generic.viewing').' Starcitizen.it')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <!--<i class="icon voyager-window-list"></i> {!! $feed->get_title() !!}-->
            <img src="{!! $feed->get_image_url() !!}"> {!! $feed->get_title() !!}
        </h1>
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-bordered">
            <div class="panel-body table-responsive">
                <div class="sc-items">
                    @foreach ($feed->get_items() as $item)
                    <div class="sc-item">
                        <h4>
                            <a href="{!! route('voyager.starcitizen.show', $loop->index) !!}">
                            {{ $item->get_title() }}<br><small>{{ $item->get_date() }}</small>
                            </a>
                        </h4>
                        <p>{!! $item->get_description() !!}</p>
                    </div>
                    <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
@parent
<style type="text/css">
.table-feeds a {
    text-decoration: none;
}
.input-group-btn .btn {
    margin-top: -1px;
}
</style>
@stop

@section('javascript')
@stop
