@extends('voyager::master')

@section('page_title', __('voyager.generic.viewing').' Feeds')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="icon voyager-window-list"></i> Feeds
        </h1>
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body table-responsive">
                        <div class="row">
                            <div class="col-sm-3 col-md-2">
                                <form id="form-cats" role="search" method="get" action="{!! url('admin/feeds') !!}" data-pjax>
                                    <select name="c" class="form-control" onchange="$('#form-cats').submit();">
                                        <option value="">All categories</option>
                                        @foreach ($categories as $category)
                                            @if ($category->id == request()->get('c'))
                                                <option selected value="{{ $category->id }}">{{ $category->name }}</option>
                                            @else
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </form>
                            </div>
                            <div class="col-sm-4 col-md-3">
                                <form role="search" method="get" action="{!! url('admin/feeds') !!}" data-pjax>
                                    <input type="hidden" name="a" value="{{ request('a', '') }}">
                                    <input type="hidden" name="c" value="{{ request('c', '') }}">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input name="q" type="text" class="form-control" id="navbar-search-input"
                                                    placeholder="Search" value="{!! Request::get('q', '') !!}">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit">Go!</button>
                                            </span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        {!! print_filter('q') !!}
                        {!! print_filter('c') !!}
                        {!! print_filter('a') !!}
                        <br><br>

                        @if (count($feeds) > 0)
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-feeds">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Category</th>
                                        <th>Date</th>
                                        <th>Size</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody class="tbody">
                                @foreach ($feeds as $feed)
                                    <tr class="tr">
                                        <td>
                                            @if ($feed->content != null)
                                                <a href="{{ url('/admin/feeds', $feed->slug) }}">{{ $feed->title }}</a>
                                            @else
                                                <a target="_blank" href="{{ $feed->link }}">{{ $feed->title }}</a>
                                            @endif
                                        </td>
                                        <td><a href="{!! get_filter('a', $feed->author) !!}">{{ $feed->author }}</a></td>
                                        <td><a data-pjax href="{!! get_filter('c', $feed->category->id) !!}">{{ $feed->category->name }}</a></td>
                                        <td>{{ $feed->pubDate->format('d/m/Y H:i:s') }}</td>
                                        <td class="text-right nowrap">
                                            @if ($feed->enclosure_length)
                                                    {!! human_filesize($feed->enclosure_length) !!}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @if ($feed->enclosure_type === 'application/x-bittorrent')
                                                <a href="{!! $feed->enclosure_url !!}" target="_blank">
                                                    <i class="icon voyager-download" aria-hidden="true"></i>
                                                </a>
                                            @else
                                                <a href="{!! $feed->link !!}" target="_blank"><i class="icon voyager-double-right" aria-hidden="true"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {!! $feeds->links() !!}
                        @else
                        <p>No records found.</p>
                        <form target="_blank" action="https://ilcorsaronero.info/argh.php" method="get">
                            <div class="form-group">
                                <input class="form-control" name="search" value="{!! Request::get('q', '') !!}" type="text">
                            </div>
                            <input class="btn btn-default" type="submit" value="cerca" alt="Submit">
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
@parent
<style type="text/css">
.table-feeds a {
    text-decoration: none;
}
.input-group-btn .btn {
    margin-top: -1px;
}
</style>
@stop

@section('javascript')
<script src="{!! asset('js/plugins/infinite-scroll.pkgd.min.js') !!}"></script>
<script>
$('.table-feeds .tbody').infiniteScroll({
    path: '.pagination a[rel=next]',
    append: '.tr',
    history: false,
});
</script>
@stop
