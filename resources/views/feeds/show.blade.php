@extends('voyager::master')

@section('page_title', 'Feeds - '.$feed->title)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="icon voyager-puzzle"></i> {{ $feed->title }} 
            <a href="{!! $feed->link !!}" target="_blank"><i class="icon voyager-magnet"></i></a>
        </h1>
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body table-responsive">
                        <div class="item">
                            {!! $feed->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
@parent
<style type="text/css">
.codecolorer-container {
    width: 100% !important;
    margin: 0px !important;
}
</style>
@stop

@section('javascript')
@parent
<script>
$('.panel-body:first .item:first img').addClass('wow fadeIn');
$(document).ready(function(){
    $('.item img').addClass('img-responsive');
    $('.item iframe').width('100%');
});
</script>
@stop