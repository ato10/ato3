@extends('voyager::master')

@section('page_title', 'Transmission')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="icon voyager-window-list"></i> Transmission
        </h1>
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body table-responsive">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-torrents">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>Name</th>
                                        <th>&nbsp;</th>
                                        <th>Download Rate</th>
                                        <th>UploadRate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($torrents as $torrent)
                                    <tr {!! $torrent->isFinished() ? 'class="success"' : "" !!}>
                                        <td>
                                        @if($torrent->isStopped())
                                            <a href="{!! route('voyager.transmission.start', $torrent->getHash()) !!}">
                                                <div class="icon voyager-play"></div>
                                            </a>
                                        @else
                                            <a href="{!! route('voyager.transmission.stop', $torrent->getHash()) !!}">
                                                <div class="icon voyager-pause"></div>
                                            </a>
                                        @endif
                                        </td>
                                        <td><strong>{{ $torrent->getName() }}</strong></td>
                                        <td class="text-right active">{{ $torrent->getPercentDone() }}%</td>
                                        <td class="text-right">{{ human_filesize($torrent->getDownloadRate(), 2) }}/s</td>
                                        <td class="text-right">{{ human_filesize($torrent->getUploadRate(), 2) }}/s</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
@parent
<style type="text/css">
.table-torrents a {
    text-decoration: none;
}
</style>
@stop

@section('javascript')
@stop
